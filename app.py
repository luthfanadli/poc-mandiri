from configparser import LegacyInterpolation
from msilib import MSIDBOPEN_DIRECT
from tkinter import HORIZONTAL
from turtle import left, right
import streamlit as st
import pandas as pd
import geopandas as gpd
import plotly.express as px
import folium
from sklearn.preprocessing import MinMaxScaler
from streamlit_folium import st_folium,  folium_static   
from folium.plugins import Draw
from shapely.geometry import Point
import altair as alt

def drivingtime(longitude,latitude, identify, dt, method = 'distance', vehicle = 'car'):
    """Fungsi untuk melakukan catchment menggunakan driving time sejauh dt
    Parameters
    ----------

    longitude : int
        koordinat x pada point
    latitude : int
        koordinat y pada point
    identify : str
        nama colom sebagai identify data titik (primery key)
    dt : int
        Distance for catchmnet area in meters
    method : str
        menggunakan time atau distance (jika None maka menggunakan default atau distance)
    vehicle : str
        Jenis kendaraan yang digunakan car,truck,atau motorcycle. jika None maka menggunakan default atau car
            
    ----------
    """
    list_x = [longitude]
    list_y = [latitude]
    list_id = [identify]
        
    list_drivetime = []
    for x, y, z in zip(list_x, list_y, list_id):
        try:
            drivetime = gpd.read_file("http://api-la.lokasi.site/api/v1.0/routing/isochrone/?point={0},{1}&reverse_flow=true&{2}_limit={3}&profile={4}".format(x,y,method,dt,vehicle))
        except:
            drivetime = gpd.read_file("http://13.212.107.88:8989/api/v1.0/routing/isochrone/?point={0},{1}&reverse_flow=true&{2}_limit={3}&profile={4}".format(x,y,method,dt,vehicle))
        drivetime['id'] = z
        list_drivetime.append(drivetime)
    drive_time = pd.concat(list_drivetime).drop(columns='bucket')
    return drive_time

def vizMap(poi, poly,col_name,title):
    
    """
    poi:point of interest geodataframe
    poly:polygon geodataframe
    col_name:nama kolom yg akan di plot 
    title:Judul
    map_name:nama peta untuk di simpan
    
    """
    #Map Visualization
    m = poly.explore(
        color = 'grey',
        tiles='CartoDB Positron',
        legend = True,
        popup = False,
    )

    poi.explore(m=m,
        column = col_name,
        cmap = 'OrRd',
        legend=True,
        marker_kwds=dict(radius=5, fill=True),
        popup = False,
    )
    
    #Add Titile
   
    title_html = '''
             <h3 align="center" style="font-size:16px"><b>{}</b></h3>
             '''.format(title) 
    m.get_root().html.add_child(folium.Element(title_html))

    return m

kams={'Cabang Menara':[-6.109871, 106.738968], 
'Kramatjati':[-6.295201, 106.870199]}


st.set_page_config(page_title='POI RECOMENDATION DASBOARD',
                    page_icon=":bar_chart:",
                    layout="wide")

st.title(":bar_chart: Map Dasboard")
st.sidebar.header("Filter: ")

area=st.sidebar.selectbox(
    "Select Branch",
    options=['Kramatjati','Cabang Menara']
    )

loc=kams.get(area)
x,y=(loc[1],loc[0])

poly=drivingtime(x,y,  0, 5000, method = 'distance', vehicle = 'car')


if loc:
    
    m = folium.Map(location=loc, zoom_start=11)

    folium.Marker(
                    location=loc,popup=area).add_to(m)
    #Draw(export=True).add_to(m)

    Draw(export=True,

    position='topleft',
    draw_options={'polyline': {'allowIntersection': False}},
    edit_options={'poly': {'allowIntersection': False}}).add_to(m)

    st_folium(m, width = 1000, height=600)


if area=='Kramatjati':

    df=pd.read_parquet("cabang_menara.parquet")
else:
    df=pd.read_parquet("keramatjati.parquet")


bs=st.sidebar.multiselect(
    "Bisnis Size",
    options=['Micro', 'Business Banking', 'Wholesale']
    )

df1=df.query(
    "business_size==@bs").reset_index(drop=True)

group=st.sidebar.multiselect(
    "Bisnis by Group",
    options=df1.group_name.unique()
    )

    

st.sidebar.subheader("Inputs Parameter")

if st.sidebar.checkbox("Show advanced options"):
    par=st.sidebar.multiselect("Select Parameter", options=['access', 'u15_u55','rating', 'reviewers', 'mobile data (avg)'])

    for feature in par:
        
        skoring=st.sidebar.number_input('Skoring '+ feature , 0,100)

        min_selection, max_selection = st.sidebar.slider("Distance "+ feature, min_value=0, max_value=10000, value=[0, 10000])
        
        positive_neg = st.sidebar.radio('Influence '+feature,('Positive', 'Negative'))
        

st.write('<style>div.row-widget.stRadio > div{flex-direction:row;}</style>', unsafe_allow_html=True)


    

df_selection=df1.query(
    "group_name==@group").dropna().reset_index(drop=True)



left_column, midlle_column, right_column =st.columns(3)

if group:


    with left_column:
        st.subheader('Bisnis Size:',)
        st.subheader(f'Jumlah Data {len(df1):,}')

    with midlle_column:
        st.subheader('Group Name:')
        st.subheader(f'Jumlah Data {len(df_selection):,}')

    with right_column:
        st.subheader('Average:')
        st.subheader(f'{len(df_selection)/len(df1):,} %')

    st.markdown('---')

    st.dataframe(df_selection)

    st.title(":bar_chart: Data Dasboard")
    st.markdown("##")

    data=df_selection.copy()
    data2=['access', 'u15_u55','rating', 'reviewers', 'mobile data (avg)']
    dataaa=[20,20,20,20,20]
        
    scaler=MinMaxScaler()
    data[data2] = scaler.fit_transform(data[data2])
    rockam=dict(zip(data2, dataaa))

    for i, j in enumerate(data2):
        data[j]=round(data[j]*rockam[j], 3)
 
    df2=data.copy()
    df2['total']=df2[data2].sum(axis = 1, skipna = True)
    df2=df2.sort_values(by='total', ascending=False).head(10).reset_index(drop=True)

    st.dataframe(df2.drop(columns=['longitude','latitude'])\
        .style.format({"access": "{:0,.0f}", 
                          "u15_u55": "{:0,.0f}", 
                          "rating": "{:0,.0f}",
                          "mobile data (avg)":"{:0,.0f}",
                          "total":"{:0,.0f}"})\
                 .hide_index()\
                 .background_gradient(cmap='Blues'))

    df2['geom']=[Point(x,y) for x,y in zip(df2['longitude'],df2['latitude'])]

    df2=df2.set_geometry('geom')

    m1=vizMap(df2, poly,'Brand',area)

   
    st.title(":bar_chart: Result Dasboard")
   
    #st.altair_chart(m1)
    st_folium(m1,width = 1200, height=600)



#TOP KPI's
# if len(df_selection)>1:


#     total_af=int(df_selection.dropna()['AF'].sum())
#     max_period=df_selection.sort_values(by='AF', ascending=False).head(1)['periode'].values[0]
#     max_af=df_selection.sort_values(by='AF', ascending=False).head(1)['AF'].values[0]
#     average_af=round(df_selection.dropna()['AF'].mean())


#     left_column, midlle_column, right_column =st.columns(3)

#     with left_column:
#         st.subheader('Total Amount Finance:',)
#         st.subheader(f'Rp. {total_af:,}')

#     with midlle_column:
#         st.subheader('Max Period:')
#         st.subheader(f'{max_period }, Rp.{int(max_af):,}')

#     with right_column:
#         st.subheader('Average:')
#         st.subheader(f'Rp. {average_af:,}')

#     st.markdown('---')

#     print(area)

# af_by_zipcode=df_selection.groupby(by=['periode']).sum()[['AF']].sort_values(by='AF')



# fig_af=px.area(
#     df_selection,
#     x='periode',
#     y='AF',
#     line_group='Kode Pos',color='Kode Pos',

#     orientation='h',
#     title="<b>Amount Finance <b>"

# )

# fig_af.update_layout(
#     plot_bgcolor="rgba(0,0,0,0)",
#     xaxis=(dict(showgrid=False))
#                     )


# fig_bar=px.bar(
#     df_selection,
#     x='periode',
#     y='AF',color='Kode Pos',    

#     title="<b>Amount Finance <b>"
#     #color_discrete_sequence=["#0083B8"] * len(af_by_zipcode)
#     #template="plotly_white"

# )

# fig_bar.update_layout(
#     plot_bgcolor="rgba(0,0,0,0)",
#     xaxis=(dict(showgrid=False))
#                     )

# #st.plotly_chart(fig_bar)

# left_column, right_column =st.columns(2)

# left_column.plotly_chart(fig_af, use_container_width=True)
# right_column.plotly_chart(fig_bar, use_container_width=True)

# df = px.data.election()
# geojson = px.data.election_geojson()

# fig = px.choropleth_mapbox(df, geojson=geojson, color="Bergeron",
#                            locations="district", featureidkey="properties.district",
#                            center={"lat": 45.5517, "lon": -73.7073},
#                            mapbox_style="carto-positron", zoom=9)


# st.plotly_chart(fig)


# map_heatmap = folium.Map(location=[48.1351, 11.5820], zoom_start=11)
# folium_static(map_heatmap)


# # ---- HIDE STREAMLIT STYLE ----
# hide_st_style = """
#             <style>
#             #MainMenu {visibility: hidden;}
#             footer {visibility: hidden;}
#             header {visibility: hidden;}
#             </style>
#             """
# st.markdown(hide_st_style, unsafe_allow_html=True)



